package steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import dataBase.DoctorsListsBase;
import helpers.DataBaseHelper;
import helpers.TableHelper;
import io.cucumber.java.bg.И;
import java.io.FileNotFoundException;
import lombok.SneakyThrows;
import org.aeonbits.owner.ConfigFactory;
import utils.Sleep;
import utils.WordUtil;
import utils.properties.Configurations;

import java.util.List;

import static com.codeborne.selenide.Selenide.*;

public class CheckMarking {

    DataBaseHelper dataBaseHelper = new DataBaseHelper();
    DoctorsListsBase doctorsListsBase = new DoctorsListsBase();
    TableHelper tableHelper = new TableHelper();
    BaseActionSteps baseActionSteps = new BaseActionSteps();
    Configurations conf = ConfigFactory.create(Configurations.class, System.getProperties());
    WordUtil wordDoc;
    @SneakyThrows
    @И("Проверить наличие маркировки и упаковки в городе {string}")
    public void searchMarkAndTare(String city) throws FileNotFoundException {
        wordDoc = new WordUtil("Маркировка и упаковка с " + conf.getFirstDate() + " по " + conf.getSecondDate() + " " + city + ".docx");
        wordDoc.createFile();
        List docktorsList = doctorsListsBase.getListByCity(city);
        for (int i = 0; i < docktorsList.size(); i++){
            System.out.println(docktorsList.get(i));
            wordDoc.setTextInDoc(String.valueOf(docktorsList.get(i)));
            wordDoc.setTextInDoc("================================================");
            $x("//span[@id='findFormTop']").click(); // нажатие на кнопку Поиск
            $x("//span[@id='select2-findFormUser-container']").click(); //нажатие на поле Ветринарный врач
            $x("//input[@class='select2-search__field']").setValue((String) docktorsList.get(i));// ввод ФИО врача
            $x("//li[@class='select2-results__option select2-results__option--highlighted']").click();//выбор врача из строки поиска
            Sleep.pauseSec(2);
            $x("//button[contains(.,'Найти')]").should(Condition.visible).click(); //нажатие на кнопку Найти
            Sleep.pauseSec(2);
            if ($x("//div[@id='listContent']").getText().equals("Список пуст")) {
                System.out.println(docktorsList.get(i) + " - нет Исходящих ВСД за заданный период");
                continue;
            }
            int pageCount = baseActionSteps.getCountPage();
            for (int j = 1; j <= pageCount ; j++) {

                $x("//span[@id='printSettingsFormTop']").click();// нажатие на кнопку Печать
                $x("//input[@id='printTypeLayoutHeaderRadio']").click();
                $x("//input[@id='printSchemaLayoutHeaderRadio']").click(); // нажатие на радиокнопку Печать журнала ВСД
                $x("//select/option[contains(text(),'Шаблон для проверки1')]").click();// выбор шаблона
                Sleep.pauseSec(3);
                while ($x("//button[contains(.,'Сформировать')]").getAttribute("disabled") != null) {
                    Sleep.pauseSec(10);
                }
                $x("//button[contains(.,'Сформировать')]").should(Condition.visible).click();// нажатие кнопки Сформировать
                Sleep.pauseSec(3);
                switchTo().window(1);// перейти к активному окну
                checkValuesInColumnMarkirovka("маркировка"); // проверить наличие слова Маркировка в столбце
                closeWindow();
                switchTo().window(0);//перейти к активному окну

                SelenideElement buttonNext = $x("//a[text()='Следующая']"); // переход на следующую страницу
                if (buttonNext.isDisplayed()) {
                    buttonNext.click();
                    System.out.println("-----------------------------------------");
                    System.out.println("Проверка страницы " + (j) + " окончена");
                    System.out.println("-----------------------------------------");
                    Sleep.pauseMs(800);
                } else {
                    continue;
                }
            }
        }
        wordDoc.recordFile();
    }

    /**
     * Проверка колонки Маркировка
     * @param arg
     */
    public void checkValuesInColumnMarkirovka(String arg){
        ElementsCollection rows = tableHelper.getRowsInTable();
        for (int i = 0; i < rows.size(); i++) {
            String result = rows.get(i).scrollIntoView(true).$$x(".//td").get(tableHelper.getColumnIndex("Упаковка")).getText();
            if (rows.get(i).$x(".//td[5]").getText().equals("кошка") || rows.get(i).$x(".//td[5]").getText().equals("собака") || rows.get(i).$x(".//td[5]").getText().equals("лошадь спортивная") || rows.get(i).$x(".//td[5]").getText().equals("биоотходы") || rows.get(i).$x(".//td[5]").getText().equals("суточный цыплёнок")){
                continue;
            }else if (result.equals("")) {
                System.out.println("==========================================================================");
                System.out.println("Нет упаковки и маркировки у ВСД №" + rows.get(i).$x(".//td[2]").getText() + " от " + rows.get(i).$x(".//td[3]").getText());
                System.out.println("Продукция: " + rows.get(i).$x(".//td[5]").getText());
                System.out.println("На предприятии : " + rows.get(i).$x(".//td[9]").getText());
                System.out.println("==========================================================================");
                wordDoc.setTextInDoc("Нет упаковки и маркировки у ВСД №" + rows.get(i).$x(".//td[2]").getText() + " от " + rows.get(i).$x(".//td[3]").getText());
                wordDoc.setTextInDoc("Вид продукции: " + rows.get(i).$x(".//td[4]").getText());
                wordDoc.setTextInDoc("Продукция: " + rows.get(i).$x(".//td[5]").getText());
                wordDoc.setTextInDoc("ХС : " + rows.get(i).$x(".//td[9]").getText());
                wordDoc.setTextInDoc("На предприятии : " + rows.get(i).$x(".//td[10]").getText());
                wordDoc.addIndent();
            }else if (!result.contains(arg)){
                System.out.println("==========================================================================");
                System.out.println("Отсутствует маркировка в ВСД №" + rows.get(i).$x(".//td[2]").getText() + " от " + rows.get(i).$x(".//td[3]").getText());
                System.out.println("Продукция: " + rows.get(i).$x(".//td[4]").getText());
                System.out.println("На предприятии : " + rows.get(i).$x(".//td[9]").getText());
                System.out.println("==========================================================================");
                wordDoc.setTextInDoc("Отсутствует маркировка в ВСД №" + rows.get(i).$x(".//td[2]").getText() + " от " + rows.get(i).$x(".//td[3]").getText());
                wordDoc.setTextInDoc("Вид продукции: " + rows.get(i).$x(".//td[4]").getText());
                wordDoc.setTextInDoc("Продукция: " + rows.get(i).$x(".//td[5]").getText());
                wordDoc.setTextInDoc("ХС : " + rows.get(i).$x(".//td[9]").getText());
                wordDoc.setTextInDoc("На предприятии : " + rows.get(i).$x(".//td[10]").getText());
                wordDoc.addIndent();
            }
        }
    }

}
