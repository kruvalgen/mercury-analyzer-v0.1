package steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import dataBase.DoctorsListsBase;
import helpers.TableHelper;
import io.cucumber.java.ru.Тогда;
import java.io.FileNotFoundException;
import lombok.SneakyThrows;
import org.aeonbits.owner.ConfigFactory;
import utils.Sleep;
import utils.WordUtil;
import utils.properties.Configurations;

import java.util.List;

import static com.codeborne.selenide.Selenide.*;

public class CheckResearches {

    DoctorsListsBase doctorsListsBase = new DoctorsListsBase();
    TableHelper tableHelper = new TableHelper();
    BaseActionSteps baseActionSteps = new BaseActionSteps();
    WordUtil wordDoc;
    Configurations conf = ConfigFactory.create(Configurations.class, System.getProperties());

    @SneakyThrows
    @Тогда("Проверить наличие лабораторных исследований для Мяса в городе {string}")
    public void checkResearches(String city) throws FileNotFoundException {
        wordDoc = new WordUtil("Лабораторные исследования на мясную продукцию с " + conf.getFirstDate() + " по " + conf.getSecondDate() + " " + city + ".docx");
        wordDoc.createFile();
        List docktorsList = doctorsListsBase.getListByCity(city);
        for (int i = 0; i < docktorsList.size(); i++) {
            System.out.println(docktorsList.get(i));
            wordDoc.setTextInDoc(String.valueOf(docktorsList.get(i)));
            wordDoc.setTextInDoc("================================================");
            $x("//span[@id='findFormTop']").click(); // нажатие на кнопку Поиск
            $x("//span[@id='select2-findFormUser-container']").click(); //нажатие на поле Ветринарный врач
            $x("//input[@class='select2-search__field']").setValue((String) docktorsList.get(i));// ввод ФИО врача
            $x("//li[@class='select2-results__option select2-results__option--highlighted']").click();//выбор врача из строки поиска
            Sleep.pauseSec(2);
            $x("//button[contains(.,'Найти')]").should(Condition.visible).click(); //нажатие на кнопку Найти
            Sleep.pauseSec(2);
            if ($x("//div[@id='listContent']").getText().equals("Список пуст")) {
                System.out.println(docktorsList.get(i) + " - нет Исходящих ВСД за заданный период");
                continue;
            }
            int pageCount = baseActionSteps.getCountPage();
            for (int j = 1; j <= pageCount; j++) {
                Sleep.pauseSec(2);
                $x("//span[@id='printSettingsFormTop']").click();// нажатие на кнопку Печать
                $x("//input[@id='printTypeLayoutHeaderRadio']").click();
                $x("//input[@id='printSchemaLayoutHeaderRadio']").click(); // нажатие на радиокнопку Печать журнала ВСД
                $x("//select/option[contains(text(),'Лабораторные исследования2')]").click();// выбор шаблона
                Sleep.pauseSec(3);
                while ($x("//button[contains(.,'Сформировать')]").getAttribute("disabled") != null) {
                    Sleep.pauseSec(10);
                }
                $x("//button[contains(.,'Сформировать')]").should(Condition.visible).click();// нажатие кнопки Сформировать
                Sleep.pauseSec(3);
                switchTo().window(1);// перейти к активному окну
                checkValuesInColumnResearches();
                closeWindow();
                switchTo().window(0);//перейти к активному окну

                SelenideElement buttonNext = $x("//a[text()='Следующая']"); // переход на следующую страницу
                if (buttonNext.isDisplayed()) {
                    buttonNext.click();
                    System.out.println("-----------------------------------------");
                    System.out.println("Проверка страницы " + (j) + " окончена");
                    System.out.println("-----------------------------------------");
                    Sleep.pauseMs(800);
                } else {
                    continue;
                }
            }
        }
        wordDoc.recordFile();
    }


    /**
     * Проверка колонки лабораторные исследования
     */
    public void checkValuesInColumnResearches() {
        ElementsCollection rows = tableHelper.getRowsInTable();
        for (int i = 0; i < rows.size(); i++) {
            String result = rows.get(i).scrollIntoView(true).$$x(".//td")
                .get(tableHelper.getColumnIndex("Результат лабораторного исследования")).getText();// столбец Результаты лаб исследований
            boolean result2 = rows.get(i).$x(".//td[7]").getText().equals("изготовлена из сырья, прошедшего ветеринарно-санитарную экспертизу");// столбец ВетСанЭкспертиза
            String result3 = rows.get(i).$x(".//td[10]").getText(); // столбец Особые отметки
            String result4 = rows.get(i).$x(".//td[11]").getText(); // столбец Примечания
            if (result.equals("") && !result2 && result3.equals("") && result4.equals("")) {
                System.out.println(" Лабораторные исследования в ВСД " + rows.get(i).$$x(".//td").get(
                    tableHelper.getColumnIndex("Номер ВСД")).getText() + " от "
                        + rows.get(i).$$x(".//td").get(tableHelper.getColumnIndex("Дата оформления"))
                    .getText().split(" ")[0] + " отсутствуют." );
                System.out.println("Вид продукции - " + rows.get(i).$x(".//td[4]").getText());
                System.out.println("Продукция - " + rows.get(i).$x(".//td[5]").getText());
                System.out.println("Предприятие - " + rows.get(i).$x(".//td[8]").getText());
                System.out.println("______________________________________________________________");
                wordDoc.setTextInDoc(" Лабораторные исследования в ВСД " + rows.get(i).$$x(".//td").get(
                    tableHelper.getColumnIndex("Номер ВСД")).getText() + " от "
                    + rows.get(i).$$x(".//td").get(tableHelper.getColumnIndex("Дата оформления")).getText().split(" ")[0] + " отсутствуют." );
                wordDoc.setTextInDoc("Вид продукции - " + rows.get(i).$x(".//td[4]").getText());
                wordDoc.setTextInDoc("Продукция - " + rows.get(i).$x(".//td[5]").getText());
                wordDoc.setTextInDoc("ХС - " + rows.get(i).$x(".//td[8]").getText());
                wordDoc.setTextInDoc("Предприятие - " + rows.get(i).$x(".//td[9]").getText());
                wordDoc.addIndent();
            }
        }
    }
}
