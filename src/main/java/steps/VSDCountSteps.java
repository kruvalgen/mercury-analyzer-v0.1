package steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import dataBase.Cityes.PyatigorskDocList;
import io.cucumber.java.ru.Когда;
import java.util.List;
import org.aeonbits.owner.ConfigFactory;
import utils.properties.Configurations;

import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class VSDCountSteps {

  Configurations conf = ConfigFactory.create(Configurations.class, System.getProperties());
  SelenideElement searchButton = $x("//span[@id='findFormTop']");
  SelenideElement printButton = $x("//span[@id='printSettingsFormTop']");

  SelenideElement checkBox = $x("//input[@type='checkbox'][@value='4']");
  SelenideElement searchDoktor = $x("//span[@id='select2-findFormUser-container']");
  SelenideElement enterDoktorSurName = $x("//input[@class='select2-search__field']");
  String doc = $x("//div[@id='loggedas']/b").getText();
  String[] doct = doc.split("\\(");
  SelenideElement clickSearchResult =
      $x("//li[@class='select2-results__option select2-results__option--highlighted']");

  @Когда("получить количество ВСД")
  public void wiringList() {
    ElementsCollection organization = $$x("//input[@type='radio']");
    for (int i = 0; i < organization.size(); i++) {
      organization.get(i).click();
      Selenide.$x("//button[contains(.,'Выбрать')]").scrollIntoView(true).click();
      Selenide.$x("//a[contains(.,'Ветеринарные документы')]").click();
      Selenide.$x("//a[contains(.,'Исходящие')]").click();
      searchButton.click();
      checkBox.shouldBe(Condition.visible).click();
      searchDoktor.click();
      enterDoktorSurName.setValue(doct[0]);
      clickSearchResult.click();
      Selenide.$x("//input[@data-to='vetDocumentDateToId']").setValue("28.06.2024");
      Selenide.$x("//input[@data-from='vetDocumentDateId']").setValue("30.07.2024");
      Selenide.$x("//button[contains(.,'Найти')]").shouldBe(Condition.visible).click();
      Selenide.$x("//span[@title='Показать количество']").click();
      String countVSD = $x("//span[@id='totalSizeView']").text().substring(9);
      String count = countVSD.split("\\)")[0];
      String organizationRes = $x("//a/b").text();

      Selenide.$x("//a[normalize-space()='Производственные']").click();
      searchButton.click();
      checkBox.shouldBe(Condition.visible).click();
      searchDoktor.click();
      enterDoktorSurName.setValue(doct[0]);
      clickSearchResult.click();
      Selenide.$x("//input[@data-to='vetDocumentDateToId']").setValue("28.06.2024");
      Selenide.$x("//input[@data-from='vetDocumentDateId']").setValue("30.07.2024");
      Selenide.$x("//button[contains(.,'Найти')]").shouldBe(Condition.visible).click();
      Selenide.$x("//span[@title='Показать количество']").click();
      String countSecondVSD = $x("//span[@id='totalSizeView']").text().substring(9);
      String countSecond = countSecondVSD.split("\\)")[0];

      if (!count.equals(" 0") || !countSecond.equals(" 0")) {
        System.out.println(organizationRes + " =" + count + " =" + countSecond);
      }
      Selenide.sleep(3000);
      Selenide.$x("//a[contains(.,'Сменить предприятие')]").should(Condition.visible).click();
    }
  }

  @Когда("Получение кол-ва ВСД по ветврачу Пятигорск")
  public void countVSDByDoc() {
    List<String> list = PyatigorskDocList.getPyatigorskList();

    for (int i = 0; i < list.size(); i++) {
      Selenide.$x("//a[contains(.,'Ветеринарные документы')]").click();
      Selenide.$x("//a[contains(.,'Исходящие')]").click();
      $x("//span[@id='findFormTop']").click(); // нажатие на кнопку Поиск
      $x("//input[@name='vetDocumentDate']").setValue(conf.getFirstDate()); //ввод первой даты
      $x("//input[@name='vetDocumentDateTo']").setValue(conf.getSecondDate()); //ввод второй даты
      $x("//input[@id='findUserFormedPaper']").click(); //нажать чекбокс Кто оформил

      searchDoktor.click();
      enterDoktorSurName.setValue(list.get(i));
      clickSearchResult.click();
      Selenide.$x("//button[contains(.,'Найти')]").shouldBe(Condition.visible).click();
      Selenide.$x("//span[@title='Показать количество']").click();
      String countFirstVSD = $x("//span[@id='totalSizeView']").text().substring(9);
      String countFirst = countFirstVSD.split("\\)")[0];

      Selenide.$x("//a[normalize-space()='Производственные']").click();

      $x("//span[@id='findFormTop']").click(); // нажатие на кнопку Поиск
      $x("//input[@name='vetDocumentDate']").setValue(conf.getFirstDate()); //ввод первой даты
      $x("//input[@name='vetDocumentDateTo']").setValue(conf.getSecondDate()); //ввод второй даты
      $x("//input[@id='findUserFormedPaper']").click(); //нажать чекбокс Кто оформил

      searchDoktor.click();
      enterDoktorSurName.setValue(list.get(i));
      clickSearchResult.click();
      Selenide.$x("//button[contains(.,'Найти')]").shouldBe(Condition.visible).click();
      Selenide.$x("//span[@title='Показать количество']").click();
      String countSecondVSD = $x("//span[@id='totalSizeView']").text().substring(9);
      String countSecond = countSecondVSD.split("\\)")[0];

      if (!countFirst.equals(" 0") || !countSecond.equals(" 0")) {
        System.out.println(list.get(i) + " =" + countFirst + " =" + countSecond);
      }

      Selenide.sleep(3000);

    }
  }
}
