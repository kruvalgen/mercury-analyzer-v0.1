package steps;

import com.codeborne.selenide.Condition;
import io.cucumber.java.bg.И;
import org.aeonbits.owner.ConfigFactory;
import utils.properties.Configurations;

import static com.codeborne.selenide.Selenide.$x;

public class SettingSteps {
    Configurations conf = ConfigFactory.create(Configurations.class, System.getProperties());

    @И("Нажать на кнопку Поиск, отметить чекбокс Кто оформил")
    public void clickButtonSearchAndEnterDates(){
        $x("//select[@name='rows']/option[@value='100']").click();//выбрать 100 записей для отображения
        $x("//span[@id='findFormTop']").click(); // нажатие на кнопку Поиск
        $x("//input[@name='vetDocumentDate']").setValue(conf.getFirstDate()); //ввод первой даты
        $x("//input[@name='vetDocumentDateTo']").setValue(conf.getSecondDate()); //ввод второй даты
        $x("//input[@id='findUserFormedPaper']").click(); //нажать чекбокс Кто оформил
        $x("//button[contains(.,'Найти')]").should(Condition.visible).click(); //нажатие на кнопку Найти
    }

    @И("Нажать на кнопку Поиск, отметить чекбокс Кто погасил")
    public void clickButtonSearchAndEnterDates2(){
        $x("//select[@name='rows']/option[@value='100']").click();//выбрать 100 записей для отображения
        $x("//span[@id='findFormTop']").click(); // нажатие на кнопку Поиск
        $x("//input[@name='vetDocumentDate']").setValue(conf.getFirstDate()); //ввод первой даты
        $x("//input[@name='vetDocumentDateTo']").setValue(conf.getSecondDate()); //ввод второй даты
        $x("//input[@id='findUserQuenched']").click(); //нажать чекбокс Кто погасил
        $x("//button[contains(.,'Найти')]").should(Condition.visible).click(); //нажатие на кнопку Найти
    }

    @И("Нажать на кнопку Поиск, отметить чекбокс Кто оформил, выбрать Мясо и мясные продукты")
    public void clickButtonSearchChooseMeatAndEnterDates(){
        $x("//select[@name='rows']/option[@value='100']").click();//выбрать 100 записей для отображения
        $x("//span[@id='findFormTop']").click(); // нажатие на кнопку Поиск
        $x("//input[@name='vetDocumentDate']").setValue(conf.getFirstDate()); //ввод первой даты
        $x("//input[@name='vetDocumentDateTo']").setValue(conf.getSecondDate()); //ввод второй даты
        $x("//input[@id='findUserFormedPaper']").click(); //нажать чекбокс Кто оформил
        $x("//td[text()='Информация о продукции']").click(); //нажать на фильтр Информация о продукции
        $x("//td[@id='findFormProductTypeTd']").click(); //нажать на выпадающий список Тип продукции
        $x("//option[text()='Мясо и мясопродукты']").click(); //выбрать Мясо и мясопродукты
        $x("//button[contains(.,'Найти')]").should(Condition.visible).click(); //нажатие на кнопку Найти

    }

}
