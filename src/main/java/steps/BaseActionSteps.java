package steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selectors;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import helpers.DataBaseHelper;
import io.cucumber.java.bg.И;
import io.cucumber.java.ru.Если;
import java.sql.SQLException;
import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class BaseActionSteps {

    DataBaseHelper dataBaseHelper = new DataBaseHelper();

    SelenideElement searchButton = $x("//span[@id='findFormTop']");



    @И("открыть главную страницу")
    public void openUrl() {

        Selenide.open("https://vetrf.ru/vetrf/mercury.html");
    }

    @И("нажать на элемент по тексту {string}")
    public void clickInButton(String linkName){
        $(Selectors.byText(linkName)).click();
    }


    @И("в поле {string} ввести {string}")
    public void setTextInField(String fieldName, String text){
        SelenideElement field = $x(String.format("//div[./label[text()='%s']]//input",fieldName));
        field.should(Condition.visible.because("Поле '" + fieldName + "' не найдено")).click();
        field.setValue(text);
    }

    @И("подождать появления текста {string} в течение {int} секунд")
    public void waitText(String text, int sec){
        $(Selectors.byText(text)).should(Condition.visible, Duration.ofSeconds(sec));
    }

    @И("получить все колонки из таблицы {string}")
    public void findDoctorsListByCity(String city){
        try {
            dataBaseHelper.getTableByCity(city);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @И("получить список всех врачей города {string}")
    public void findAllDocktorsByCity(String city) throws SQLException {
        dataBaseHelper.doctorName(city).forEach(System.out::println);


    }

    @И("сделать скриншот страницы")
    public void screenShot(){
    }

    /**
     * получение количества страниц
     * @return
     */
    public int getCountPage(){
        Selenide.$x("//span[@title='Показать количество']").click();//нажатие на кнопку Показать количество
        String s = Selenide.$x("//span[@id='totalSizeView']").getText().substring(9).split("\\)")[0].trim();
        return (Integer.parseInt(s) / 100) + 1;
    }

    @Если("закрыть страницу")
    public void closeDriver() {
        WebDriverRunner.getWebDriver().close();
    }
}
