package steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import dataBase.DoctorsListsBase;
import helpers.TableHelper;
import io.cucumber.java.ru.И;
import org.aeonbits.owner.ConfigFactory;
import utils.Sleep;
import utils.WordUtil;
import utils.properties.Configurations;

import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.codeborne.selenide.Selenide.*;

public class CheckQuenchingTime {
    DoctorsListsBase doctorsListsBase = new DoctorsListsBase();
    TableHelper tableHelper = new TableHelper();
    BaseActionSteps baseActionSteps = new BaseActionSteps();

    LocalDate dateTime = LocalDate.now();
    WordUtil wordDoc;
    Configurations conf = ConfigFactory.create(Configurations.class, System.getProperties());




    @И("Проверить время оформления и гашения в городе {string}")
    public void checkTimeQuenching(String city) throws FileNotFoundException {
        wordDoc = new WordUtil("Время оформления и гашения " + conf.getFirstDate() + " по " + conf.getSecondDate() + " " + city + ".docx");
        wordDoc.createFile();
        List docktorsList = doctorsListsBase.getListByCity(city);
        for (int i = 0; i < docktorsList.size(); i++) {
            System.out.println(docktorsList.get(i));
            wordDoc.setTextInDoc(String.valueOf(docktorsList.get(i)));
            wordDoc.setTextInDoc("================================================");
            $x("//span[@id='findFormTop']").click(); // нажатие на кнопку Поиск
            $x("//span[@id='select2-findFormUser-container']").click(); //нажатие на поле Ветринарный врач
            $x("//input[@class='select2-search__field']").setValue((String) docktorsList.get(i));// ввод ФИО врача
            $x("//li[@class='select2-results__option select2-results__option--highlighted']").click();//выбор врача из строки поиска
            Sleep.pauseSec(2);
            $x("//button[contains(.,'Найти')]").should(Condition.visible).click(); //нажатие на кнопку Найти
            Sleep.pauseSec(2);
            if (Selenide.$x("//div[@id='listContent']").getText().equals("Список пуст")) {
                System.out.println(docktorsList.get(i) + " - нет Погашенных Входящих ВСД за заданный период");
                continue;
            }
            int pageCount = baseActionSteps.getCountPage();
            for (int j = 1; j <= pageCount; j++) {

                $x("//span[@id='printSettingsFormTop']").click();// нажатие на кнопку Печать
                $x("//input[@id='printTypeLayoutHeaderRadio']").click();
                $x("//input[@id='printSchemaLayoutHeaderRadio']").click(); // нажатие на радиокнопку Печать журнала ВСД
                $x("//select/option[contains(text(),'Проверка времени1')]").click();// выбор шаблона
                Sleep.pauseSec(3);
                while ($x("//button[contains(.,'Сформировать')]").getAttribute("disabled") != null) {
                    Sleep.pauseSec(10);
                }
                $x("//button[contains(.,'Сформировать')]").should(Condition.visible).click();// нажатие кнопки Сформировать
                Sleep.pauseSec(3);
                switchTo().window(1);// перейти к активному окну
                compareTime();
                closeWindow();
                switchTo().window(0);//перейти к активному окну

                SelenideElement buttonNext = $x("//a[text()='Следующая']"); // переход на следующую страницу
                if (buttonNext.isDisplayed()) {
                    buttonNext.click();
                    System.out.println("-----------------------------------------");
                    System.out.println("Проверка страницы " + (j) + " окончена");
                    System.out.println("-----------------------------------------");
                    Sleep.pauseMs(800);
                } else {
                    continue;
                }
            }
        }
        wordDoc.recordFile();
    }


    public void compareTime() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        ElementsCollection rows = tableHelper.getRowsInTable();
        for (int i = 0; i < rows.size(); i++) {
            String firstTime = rows.get(i).$x(".//td[3]").scrollIntoView(true).getText().split(" МСК")[0];
            LocalDateTime firstDate = LocalDateTime.parse(firstTime, formatter);

            String secondTime = rows.get(i).$x(".//td[4]").getText();
            if (!secondTime.equals(""))
                secondTime = secondTime.split(" МСК")[0];
            // получение времени оформления ВСД
            LocalDateTime secondDate = LocalDateTime.parse(secondTime, formatter); // получение времени гашения ВСД
            long t = ChronoUnit.MINUTES.between(firstDate, secondDate); // разница между временем оформления и гашения в минутах

            String sender = rows.get(i).$x(".//td[8]").getText(); //отправитель
            String recipient = rows.get(i).$x(".//td[10]").getText(); //получатель
            if (sender.equals(recipient)) continue;
            if (t < 30) { // поиск времени меньше чем полчаса
                System.out.println("========================================================");
                System.out.println("Гашение ВСД после его оформления произошло через - " + t + " минут");
                System.out.println("ВСД №" + rows.get(i).$x(".//td[2]").getText());
                System.out.println("Отправитель : " + sender);
                System.out.println("Время оформления : " + rows.get(i).$x(".//td[3]").getText());
                System.out.println("Получатель : " + recipient);
                System.out.println("Время гашения : " + rows.get(i).$x(".//td[4]").getText());
                System.out.println("Вид продукции : " + rows.get(i).$x(".//td[5]").getText());
                System.out.println("Продукция : " + rows.get(i).$x(".//td[6]").getText());
                System.out.println("========================================================");
                wordDoc.setTextInDoc("Гашение ВСД " + rows.get(i).$x(".//td[2]").getText() + " произошло через " + t + " минут ");
                wordDoc.setTextInDoc("Отправитель : " + sender);
                wordDoc.setTextInDoc("Оформлено: " + rows.get(i).$x(".//td[3]").getText());
                wordDoc.setTextInDoc("Получатель : " + recipient);
                wordDoc.setTextInDoc("ХС Получателя : " + rows.get(i).$x(".//td[9]").getText());
                wordDoc.setTextInDoc("Погашено: " + rows.get(i).$x(".//td[4]").getText());
                wordDoc.setTextInDoc("Вид продукции : " + rows.get(i).$x(".//td[5]").getText());
                wordDoc.setTextInDoc("Продукция : " + rows.get(i).$x(".//td[6]").getText());
                wordDoc.addIndent();
            }
        }
    }

}
