package dataBase.Cityes;

import dataBase.DoctorsListsBase;

import java.util.ArrayList;
import java.util.List;

public class NevinnomisskDocList extends DoctorsListsBase {
    public static List getNevinnomisskList(){
        List<String> nevinnomisskayaList = new ArrayList<>();
        nevinnomisskayaList.add("Ковалев Николай Владимирович");
        nevinnomisskayaList.add("Бражников Владимир Николаевич");
        nevinnomisskayaList.add("Филиппова Елизавета Александровна");
        nevinnomisskayaList.add("Романова Светлана Анатольевна");
        nevinnomisskayaList.add("Соколова Татьяна Павловна");
        nevinnomisskayaList.add("Романова Наталья Михайловна");
        nevinnomisskayaList.add("Мареева Валентина Серафимовна");
        nevinnomisskayaList.add("Джейранова Александра Дмитриевна");
        nevinnomisskayaList.add("Третьякова Елена Яковлевна");
        nevinnomisskayaList.add("Анурова Оксана Сергеевна");
        nevinnomisskayaList.add("Баканов Денис Андреевич");
        nevinnomisskayaList.add("Бондаренко Марина Владимировна");
        nevinnomisskayaList.add("Быкадорова Инна Ивановна");
        nevinnomisskayaList.add("Рычкова Ольга Викторовна");
        nevinnomisskayaList.add("Мельникова Анна Николаевна");
        nevinnomisskayaList.add("Мирошникова Ольга Викторовна");
        nevinnomisskayaList.add("Мустакова Алина Сергеевна");
        nevinnomisskayaList.add("Питинов Иван Анатольевич");
        nevinnomisskayaList.add("Потапов Евгений Андреевич");
        nevinnomisskayaList.add("Мальцев Эдуард Александрович");
        nevinnomisskayaList.add("Сгиблов Виталий Леонидович");
        nevinnomisskayaList.add("Семендеев Роман Сергеевич");
        nevinnomisskayaList.add("Серегин Александр Андреевич");
        nevinnomisskayaList.add("Султанова Ирина Мазитовна");
        nevinnomisskayaList.add("Мытарева Инга Дмитриевна");
        nevinnomisskayaList.add("Грамматикопуло Христофор Павлович");
        nevinnomisskayaList.add("Жигалова Кристина Сергеевна");
        return nevinnomisskayaList;
    }
}
