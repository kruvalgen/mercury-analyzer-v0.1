package dataBase.Cityes;

import dataBase.DoctorsListsBase;

import java.util.ArrayList;
import java.util.List;

public class PetrovskayaDocList extends DoctorsListsBase {

    public static List getPetrovskayaList(){
        List<String> petrovskayaList = new ArrayList<>();
        petrovskayaList.add("Алферова Валентина Тимофеевна");
        petrovskayaList.add("Босых Диана Анатольевна");
        petrovskayaList.add("Брыкалов Александр Викторович");
        petrovskayaList.add("Вивтоненко Наталья Михайловна");
        petrovskayaList.add("Долбня Игорь Геннадиевич");
        petrovskayaList.add("Дронов Роман Юрьевич");
        petrovskayaList.add("Дронова Анна Владимировна");
        petrovskayaList.add("Дуплищева Валентина Ивановна");
        petrovskayaList.add("Емелин Евгений Викторович");
        petrovskayaList.add("Квочкин Дмитрий Викторович");
        petrovskayaList.add("Костючик Василий Николаевич");
        petrovskayaList.add("Курашенко Сергей Михайлович");
        petrovskayaList.add("Литовкин Андрей Викторович");
        petrovskayaList.add("Лютов Сергей Васильевич");
        petrovskayaList.add("Магамедов Магамед Гераевич");
        petrovskayaList.add("Панченко Наталья Николаевна");
        petrovskayaList.add("Пащенко Александр Григорьевич");
        petrovskayaList.add("Писаренко Елена Владимировна");
        petrovskayaList.add("Потапенко Анатолий Константинович");
        petrovskayaList.add("Сень Людмила Егоровна");
        petrovskayaList.add("Теряев Алексей Юрьевич");
        petrovskayaList.add("Филатова Елена Алексеевна");
        petrovskayaList.add("Шатеева Светлана Ивановна");
        petrovskayaList.add("Шевченко Вячеслав Викторович");
        petrovskayaList.add("Шеина Юлия Алексеевна");
        petrovskayaList.add("Шинкаренко Анатолий Петрович");
        return petrovskayaList;
    }
}
