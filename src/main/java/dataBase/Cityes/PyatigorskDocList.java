package dataBase.Cityes;

import dataBase.DoctorsListsBase;

import java.util.ArrayList;
import java.util.List;

public class PyatigorskDocList extends DoctorsListsBase {

    public static List getPyatigorskList() {
        List<String> pyatigorskList = new ArrayList<>();

        pyatigorskList.add("Гладченко Ольга Юрьевна");
        pyatigorskList.add("Гудиева Нина Ивановна");
        pyatigorskList.add("Гущин Роман Дмитриевич");
        pyatigorskList.add("Воронин Никита Михайлович");
        pyatigorskList.add("Дмитриенко Раиса Владимировна");
        pyatigorskList.add("Долгов Александр Леонидович");
        pyatigorskList.add("Жоглик Наталья Евгеньевна");
        pyatigorskList.add("Кайзер Сергей Готфридович");
        pyatigorskList.add("Лоскутова Надежда Михайловна");
        pyatigorskList.add("Мисюра Вячеслав Петрович");
        pyatigorskList.add("Мулуканова Светлана Алексеевна");
        pyatigorskList.add("Осипова Людмила Николаевна");
        pyatigorskList.add("Перевощикова Ольга Владимировна");
        pyatigorskList.add("Рылькова Алла Васильевна");
        pyatigorskList.add("Шамайская Елена Викторовна");
        pyatigorskList.add("Чегурихина Анна Александровна");
        pyatigorskList.add("Рымарев Михаил Александрович");
        pyatigorskList.add("Пономарев Иван Иванович");
        pyatigorskList.add("Соловьев Вячеслав Николаевич");
        pyatigorskList.add("Пехташева");
        pyatigorskList.add("Шаповалова");
        pyatigorskList.add("Зиновьев");
        return pyatigorskList;
    }
}
