package dataBase.Cityes;

import dataBase.DoctorsListsBase;

import java.util.ArrayList;
import java.util.List;

public class StavropolDocList extends DoctorsListsBase {
    public static List getStavropolList() {
        List<String> stavropolList = new ArrayList<>();
        stavropolList.add("Алексанова Мери Якимовна");
//        stavropolList.add("Бобринева Галина Федоровна");
        stavropolList.add("Гарькавская Людмила Васильевна");
        stavropolList.add("Дибижев Леонид Петрович");
//        stavropolList.add("Звезднев Юрий Николаевич");
        stavropolList.add("Каргаполова Светлана Николаевна");
        stavropolList.add("Копаев Виталий Иванович");
        stavropolList.add("Нужная Татьяна Владимировна");
        stavropolList.add("Павлова Александра Алексеевна");
//        stavropolList.add("Пашкова Дарья Александровна");
        stavropolList.add("Поландова Елена Анатольевна");
        stavropolList.add("Рябова Мария Владимировна");
//        stavropolList.add("Рябущенко Евгений Владимирович");
        stavropolList.add("Свазьян Ирина Алексеевна");
        stavropolList.add("Стрельникова Татьяна Анатольевна");
        stavropolList.add("Терещенкова Людмила Николаевна");
        stavropolList.add("Ушкал Анна Юрьевна");
        stavropolList.add("Черников Евгений Михайлович");
        stavropolList.add("Чернова Виктория Арленовна");
        return stavropolList;
    }
}
