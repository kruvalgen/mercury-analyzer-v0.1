package dataBase.Cityes;

import dataBase.DoctorsListsBase;

import java.util.ArrayList;
import java.util.List;

public class KislovodskDocList extends DoctorsListsBase {

    public static List getKislovodskList() {
        List<String> kislovodskList = new ArrayList<>();

//        kislovodskList.add("Глашев Азрет Тахирович");//glashev-at-220404 Glashev001
        kislovodskList.add("Клоков Борис Юрьевич");//klokov-bj-150814 oA4uD6
        kislovodskList.add("Кнухов Руслан Владимирович");//knuhov-rv-191118 Uahahai5599
        kislovodskList.add("Кондратьева Наталия Витальевна");//kondrateva-nv-150814 Er52rR5
        kislovodskList.add("Машенцева Людмила Ивановна");
        kislovodskList.add("Суладзе Лали Давидовна");//suladze-ld-190802 q8JN4mm22
//        kislovodskList.add("Коджаков Мурат Магометович");

        return kislovodskList;
    }
}
