package dataBase.Cityes;

import dataBase.DoctorsListsBase;

import java.util.ArrayList;
import java.util.List;

public class KochubeevkaDocList extends DoctorsListsBase {
    public static List getKochubeevkaList(){
        List<String> kochubeevkaList = new ArrayList<>();
        kochubeevkaList.add("Антоненко Елена Николаевна");
        kochubeevkaList.add("Белоцерковец Людмила Анатольевна");
        kochubeevkaList.add("Болотская Анжелика Александровна");
        kochubeevkaList.add("Волобуев Петр Николаевич");
        kochubeevkaList.add("Волошин Анатолий Петрович");
        kochubeevkaList.add("Дубограев Константин Петрович");
        kochubeevkaList.add("Карабак Мария Александровна");
        kochubeevkaList.add("Козаченко Татьяна Ивановна");
        kochubeevkaList.add("Коляко Ирина Юрьевна");
        kochubeevkaList.add("Куликов Александр Николаевич");
        kochubeevkaList.add("Лосникова Анна Владимировна");
        kochubeevkaList.add("Луценко Вера Ивановна");
        kochubeevkaList.add("Луценко Оксана Николаевна");
        kochubeevkaList.add("Ляпун Светлана Николаевна");
        kochubeevkaList.add("Мищенко Сергей Владимирович");
        kochubeevkaList.add("Найверт Любовь Ивановна");
        kochubeevkaList.add("Попов Олег Анатольевич");
        kochubeevkaList.add("Рахманин Сергей Николаевич");
        kochubeevkaList.add("Режило Лариса Анатольевна");
        kochubeevkaList.add("Семенченко Евгения Васильевна");
        kochubeevkaList.add("Сергиенко Ангелина Владимировна");
        kochubeevkaList.add("Соколова Валентина Михайловна");
        kochubeevkaList.add("Ткаченко Наталья Александровна");
        kochubeevkaList.add("Филонова Татьяна Валерьевна");
        kochubeevkaList.add("Чеботарева Елена Васильевна");
        kochubeevkaList.add("Шишкина Наталья Александровна");
        kochubeevkaList.add("Шкуренко Анатолий Анатольевич");
        kochubeevkaList.add("Юсупова Аида Насифулаевна");
        return kochubeevkaList;
    }
}
