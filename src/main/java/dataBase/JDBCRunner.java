package dataBase;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCRunner {

    public Statement statement;

    public Statement getStatement() throws SQLException {
        Connection connection = ConnectionManager.open();
        return connection.createStatement();
    }

    }


