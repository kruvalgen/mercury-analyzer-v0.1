package dataBase;

import dataBase.Cityes.StavropolDocList;

import java.util.ArrayList;
import java.util.List;

import static dataBase.Cityes.KislovodskDocList.getKislovodskList;
import static dataBase.Cityes.KochubeevkaDocList.getKochubeevkaList;
import static dataBase.Cityes.NevinnomisskDocList.getNevinnomisskList;
import static dataBase.Cityes.PetrovskayaDocList.getPetrovskayaList;
import static dataBase.Cityes.PyatigorskDocList.getPyatigorskList;
import static dataBase.Cityes.StavropolDocList.getStavropolList;

public class DoctorsListsBase {

    public List getListByCity(String city) {
        List list = new ArrayList<>();
        switch (city) {
            case "Ставрополь":
                list = getStavropolList();
                break;
            case "Пятигорск":
                list = getPyatigorskList();
                break;
            case "Кисловодск":
                list = getKislovodskList();
                break;
            case "Кочубеевка":
                list = getKochubeevkaList();
                break;
            case "Петровская":
                list = getPetrovskayaList();
                break;
            case "Невинномысск":
                list = getNevinnomisskList();
                break;
            default:
                System.out.println("Такого города нет в списке");
                break;
        }
        return list;
    }
}
