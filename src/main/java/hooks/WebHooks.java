package hooks;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.qameta.allure.Allure;
import java.io.IOException;

public class WebHooks {



    @Before
    public void before() throws IOException {
        Configuration.browser = "firefox";
        Configuration.timeout = 35000;
        Configuration.browserSize = "1920x1080";
        Configuration.headless = true;
        closeWebDriver();

    }


    @After
    public void after() throws IOException {
        closeWebDriver();


    }

    public void closeWebDriver() {
        try {
            Allure.addAttachment("WebDriverClose", WebDriverRunner.getWebDriver().toString());
        } catch (Exception ignore) {
        }
        WebDriverRunner.closeWebDriver();
    }

}
