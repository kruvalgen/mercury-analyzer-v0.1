package utils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public class WordUtil {

    XWPFDocument document = new XWPFDocument();
    XWPFParagraph paragraph = document.createParagraph();
    XWPFRun run = paragraph.createRun();
    FileOutputStream output;
    String fileName;

    public WordUtil(String fileName) {
        this.fileName = fileName;
    }

    public FileOutputStream createFile() throws FileNotFoundException {
        output = new FileOutputStream(fileName);
        return output;
    }

    public void setTextInDoc(String text) {
        run.setFontSize(14);
        run.setFontFamily("Times New Roman");
        run.setText(text);
        run.addBreak();
    }

    public void addIndent(){
        run.addBreak();
    }

    public void recordFile() {
        try {
            document.write(output);
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}





