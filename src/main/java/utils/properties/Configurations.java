package utils.properties;

import org.aeonbits.owner.Config;

@Config.Sources({
        "classpath:configuration.properties"
})

public interface Configurations extends Config {

    @Key("firstDate")
    @DefaultValue("")
    String getFirstDate();

    @Key("secondDate")
    @DefaultValue("")
    String getSecondDate();
}