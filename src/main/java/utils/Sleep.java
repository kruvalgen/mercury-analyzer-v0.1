package utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class Sleep {

    public static void pauseSec(double sec) {
        sleep(sec * 1000);
    }

    public static void pauseMs(double ms) {
        sleep(ms);
    }

    private static void sleep(double ms) {
        try {
            TimeUnit.MILLISECONDS.sleep((long) ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
