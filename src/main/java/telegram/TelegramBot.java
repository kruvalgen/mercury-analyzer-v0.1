package telegram;

import lombok.SneakyThrows;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import steps.BaseActionSteps;

public class TelegramBot extends TelegramLongPollingBot {

    @Override
    public String getBotUsername() {
        return "@MerkuryProjectbot";
    }

    @Override
    public String getBotToken() {
        return "5451731160:AAHN_oBwNpWT3Zosrilmi62VUu8gPAh4qes";
    }

    @Override
    @SneakyThrows
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()){
            Message message = update.getMessage();
            if (message.hasText()){
                execute(
                        SendMessage.builder()
                                .chatId(message.getChatId().toString()).text(message.getText()).build());
            }
        }
    }
    @SneakyThrows
    public static void main(String[] args) {
        TelegramBot bot = new TelegramBot();
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
        telegramBotsApi.registerBot(bot);
    }


}
