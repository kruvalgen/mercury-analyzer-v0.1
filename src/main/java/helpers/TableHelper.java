package helpers;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$$x;

public class TableHelper {


    /**
     * получение строк из таблицы
     * @return
     */
    public ElementsCollection getRowsInTable (){
        ElementsCollection rows = $$x("//tbody/tr[./td[contains(@class,'border')]]");
        return rows;
    }

    public List<String> getHeaders(){
        return $$x("//thead[@style='display: table-header-group;']//th")
            .filter(Condition.visible)
            .stream().map(c -> c.getText().trim())
            .collect(Collectors.toList());
    }

    public int getColumnIndex(String columnName){
       return getHeaders().indexOf(columnName);

    }


}
