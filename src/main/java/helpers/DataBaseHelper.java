package helpers;


import dataBase.JDBCRunner;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DataBaseHelper {
    JDBCRunner jdbcRunner = new JDBCRunner();

    public void getTableByCity(String city) throws SQLException {
        ResultSet result = jdbcRunner.getStatement().executeQuery("select * from " + city);
        int columns = result.getMetaData().getColumnCount();
        while (result.next()){
            for (int i = 0; i <= columns; i++) {
                System.out.println(i + ") " + result.getString(i));
            }
        }
    }

    public List<String> doctorName(String city) throws SQLException {
        ResultSet result = jdbcRunner.getStatement().executeQuery("select name from " + city);
        List<String> docktorsList = new ArrayList<>();
        while (result.next()){
            docktorsList.add(result.getString("name"));
        }
        return docktorsList;
    }
}
