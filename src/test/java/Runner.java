import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;


@CucumberOptions(

        features = {"src/test/resources"},
        glue = {"steps"},
        tags = "@1"
)
public class Runner extends AbstractTestNGCucumberTests {


}


