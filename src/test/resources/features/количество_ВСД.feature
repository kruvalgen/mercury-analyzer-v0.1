#language:ru
Функционал: Меркурий

  Структура сценария: Получение кол-ва ВСД
    * открыть главную страницу
    * нажать на элемент по тексту "Меркурий.ГВЭ"
    * в поле "Имя пользователя" ввести "<Логин>"
    * в поле "Пароль" ввести "<Пароль>"
    * нажать на элемент по тексту "Войти"
    * подождать появления текста "Выбор обслуживаемого предприятия" в течение 40 секунд
    * получить количество ВСД

    Примеры:
      | Логин                | Пароль       |
#    | dolgov-aa-150813         | K2p9DzE2024     |
#    | perevocshikova-ov-150813 | s3C4Mfu20214    |
#    | mulukanova-sa-150813     | Pk54Sm46        |
##    | shamajskaja-ev-160519    | Z91vXa55        |
#    | zhoglik-ne-150813        | hK75Xh2024      |
#    | osipova-ln-150813        | 56RmU6gg        |
#    | loskutova-nm-120127      | gC9xL34W2024    |
#    | gucshin-rd-170331        | H2qpX72024      |
##    | misjura-vp-200723        | Misura1966      |
#    | voronin-nm-211113        | Nikita26rus2624 |
#    | kajzer-sg-190319         | Kz5w5XQz2021    |
#    | zinovev-vg-170314        | Hfytnnf2024     |
#    | dmitrienko-rv-190829     | De4Z6a20214     |
#    | gladchenko-oy-120516     | 30507Ars24      |
#    | rymarev-ma-220621        | kA4Mt9i24       |
#    | rylkova-av-171225        | A5rm3T42024     |
#    | solovev-vn-230314        | A5z4vn7kH24     |
#    | pizhenkova-aa-150129     | AninPass014     |
#    | ponomarev-ii-210914      | U8Ci89X8        |
#    |gudieva-ni-150813         |oS5Jw2L42022     |
#    |shapovalova-in-230721 |Nastya201024|

      | knuhov-rv-191118     | Uahahai55999 |
      | kondrateva-nv-150814 | Er52rR51     |
#      | klokov-bj-150814     | Geor221015   |
      | suladze-ld-190802    | q8JN4mm221   |
      | klokova-vs-170217    | L3m8Vs2024   |
#      |                      |              |


#  knuhov-rv-191118 Uahahai55999
#  glashev-at-220404 Glashtv002
#  kondrateva-nv-150814 Er52rR51
#  klokov-bj-150814 Geor221015
#  suladze-ld-190802 q8JN4mm221
#  kodzhakov-mm-150812 H8sY8oL424
#  klokova-vs-170217 L3m8Vs2024